<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>

<ul>
    <c:forEach items="${ tracks }" var="track">
        <li>
            <a href="/Levykauppa/track?id=${ track.getId() }">
                <c:out value="${ track.getName() }" />

            </a>
        </li>

    </c:forEach>

</ul>


</body>
</html>