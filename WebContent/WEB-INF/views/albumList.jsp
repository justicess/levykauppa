<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<ul>
		<c:forEach items="${ albums }" var="album">
		<li>
			<a href="/Levykauppa/album?id=${ album.getId() }">
				<c:out value="${ album.getTitle() }" />
			
			</a>
			</li>

		</c:forEach>

	</ul>


</body>
</html>