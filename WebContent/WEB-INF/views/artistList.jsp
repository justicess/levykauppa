<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form method="POST">
    Lis�� artisti: <input name="artistName" />
    <input type="submit" />
</form>
<form method="GET">
	Etsi artistilla: <input name="searchName" />
	<input type="submit" />
	</form>
	<form method="GET">
	Etsi albumilla: <input name="searchAlbums" />
	<input type="submit" />
	</form>

	<ul>
	
		<c:forEach items="${ artists }" var="artist">
		<li>
			<a href="/Levykauppa/artist?id=${ artist.getId() }">
				<c:out value="${ artist.getName() }" />
			
			</a>
			</li>

		</c:forEach>
		<c:forEach items="${ albums }" var="album">
		<li>
			<a href="/Levykauppa/album?id=${ album.getId() }">
				<c:out value="${ album.getTitle() }" />
			
			</a>
			</li>

		</c:forEach>

	</ul>


</body>
</html>