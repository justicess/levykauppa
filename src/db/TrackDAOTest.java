package db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import models.Album;
import models.Track;

public class TrackDAOTest {
	
	private TrackDAO dao = new TrackDAO();

	@Test
	public void testFindTracksByAlbum() {
	    Album fakeAlbum = new Album(6, "Jagged little pill");
	    List<Track> tracks = dao.findTracksByAlbum(fakeAlbum);

	    assertEquals(13, tracks.size());
	    assertEquals("All I Really Want", tracks.get(0).getName());
	}
}