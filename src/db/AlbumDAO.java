package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Album;
//import models.Artist;
import models.Artist;


public class AlbumDAO {

	private final ChinookDatabase db;

	public AlbumDAO() {
		db = new ChinookDatabase();
	}

	/* public Album findAlbumByArtist(long id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;

		try {
			connection = db.connect();
			statement = connection.prepareStatement("SELECT * FROM Album JOIN Artist ON Artist.ArtistId = Album.ArtistId WHERE Artist.ArtistId = ?");
			statement.setLong(1, id);
			results = statement.executeQuery();

			if (results.next()) {
				String title = results.getString("Title");
				long AlbumId = results.getLong("AlbumID");
				return new Album(AlbumId, title);
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			db.closeAll(connection, statement, results);
		}

		return null;
	} */
	public List<Album> findAlbumsByArtist(long id) {
		List<Album> allAlbums = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;

		try {
			connection = db.connect();
			statement = connection.prepareStatement("SELECT * FROM Album JOIN Artist ON Artist.ArtistId = Album.ArtistId WHERE Artist.ArtistId = ?");
			statement.setLong(1, id);
			results = statement.executeQuery();

			while (results.next()) {
				long albumId = results.getLong("AlbumId");
				String title = results.getString("Title");

				Album a = new Album(id, title);
				allAlbums.add(a);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			db.closeAll(connection, statement, results);
		}

		return allAlbums;
	}

	 public List<Album> findAllAlbums() {
		List<Album> allAlbums = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;

		try {
			connection = db.connect();
			statement = connection.prepareStatement("SELECT * FROM Album JOIN Artist ON Artist.ArtistId = Album.ArtistId");
			results = statement.executeQuery();

			while (results.next()) {
				long id = results.getLong("AlbumId");
				String name = results.getString("Title");

				Album a = new Album(id, name);
				allAlbums.add(a);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			db.closeAll(connection, statement, results);
		}

		return allAlbums;
	} 
	public long storeAlbum(Album album, Long artistid) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet results = null;

        try {
            connection = db.connect();
            statement = connection.prepareStatement("INSERT INTO Album (Title, ArtistId) VALUES (?, ?)",
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, album.getTitle());
            statement.setLong(2, artistid);
            statement.executeUpdate();

            results = statement.getGeneratedKeys();
            if (results.next()) {
                long id = results.getLong(1);
                album.setId(id);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            db.closeAll(connection, statement, results);
        }
        return album.getId();
    }
	public List<Album> searchAlbums(String title) {
		List<Album> searchResults = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;

		try {
			connection = db.connect();
			statement = connection.prepareStatement("SELECT * FROM Album JOIN Artist ON Artist.ArtistId = Album.ArtistId WHERE lower(Title) like ?",
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, title.toLowerCase() + "%" );
			results = statement.executeQuery();

			while (results.next()) {
				long id = results.getLong("AlbumId");
				String albumTitle = results.getString("Title");

				Album a = new Album(id, albumTitle);
				searchResults.add(a);
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			db.closeAll(connection, statement, results);
		}

		return searchResults;
	}
}
