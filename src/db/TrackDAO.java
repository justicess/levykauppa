package db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Album;
import models.Track;

public class TrackDAO {

	    private ChinookDatabase db;

	    public TrackDAO() {
	        db = new ChinookDatabase();
	    }
	    
	    /* public List<Track> findTracksByAlbum(Album album) {
	    	List<Track> tracksByAlbum = new ArrayList<>();
	    	Connection connection = null;
	    	PreparedStatement statement = null;
	    	ResultSet results = null;
	    	
	    	try {
	    		connection = db.connect();
	    		statement = connection.prepareStatement("SELECT * FROM Track JOIN Album ON Album.AlbumId = Track.AlbumId WHERE AlbumId = ? ORDER BY Name");
	    		statement.setString(1,  album.getTitle());
	    		results = statement.executeQuery();
	    		
	    		while (results.next()) {
	    			long AlbumId = results.getLong("AlbumId");
	    			long TrackId = results.getLong("TrackId");
	    			String Name = results.getString("Name");
	    			
	    			Track b = new Track(album.getId(), album.getTitle());
	    			tracksByAlbum.add(b);
	    		}
	    	} catch (Exception e) {
	    		throw new RuntimeException(e);
	    	} finally {
	    		db.closeAll(connection,  statement,  results);
	    	}
	    	
	    	return tracksByAlbum;
	    } */ 
	    public List<Track> findTracksByAlbum(long id) {
			List<Track> allTracks = new ArrayList<>();
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet results = null;

			try {
				connection = db.connect();
				statement = connection.prepareStatement("SELECT * FROM Track JOIN Album ON Album.AlbumId = Track.AlbumId WHERE Album.AlbumId = ?");
				statement.setLong(1, id);
				results = statement.executeQuery();

				while (results.next()) {
					long trackid = results.getLong("TrackId");
					String name = results.getString("Name");

					Track a = new Track(id, name);
					allTracks.add(a);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				db.closeAll(connection, statement, results);
			}

			return allTracks;
		}
	    public void storeTrack(Track track) {
	        Connection connection = null;
	        PreparedStatement statement = null;
	        ResultSet results = null;

	        try {
	            connection = db.connect();
	            statement = connection.prepareStatement("INSERT INTO Track (Name) VALUES (?)",
	                    Statement.RETURN_GENERATED_KEYS);

	            statement.setString(1, track.getName());
	            statement.executeUpdate();

	            results = statement.getGeneratedKeys();
	            if (results.next()) {
	                long id = results.getLong(1);
	                track.setId(id);
	            }
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        } finally {
	            db.closeAll(connection, statement, results);
	        }
	    }
	}

