package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import models.Album;
import models.Track;
import db.AlbumDAO;
import db.TrackDAO;

@WebServlet("/album")
public class AlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private AlbumDAO albumDao = new AlbumDAO();
	private TrackDAO trackDao = new TrackDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		long albumid = Long.parseLong(request.getParameter("id"));
		// PrintWriter writer = response.getWriter();
		List<Track> tracks = trackDao.findTracksByAlbum(albumid);
		
		
			request.setAttribute("tracks", tracks);
			// writer.println(artist.getName());
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/album.jsp");
			dispatcher.include(request, response);
	}

}
