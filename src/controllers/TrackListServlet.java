package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.TrackDAO;
import db.AlbumDAO;
import models.Track;
import models.Album;

@WebServlet("/tracks")
public class TrackListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private TrackDAO trackDao = new TrackDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Track> tracks = trackDao.findTracksByAlbum(albumid);

        request.setAttribute("Tracks", tracks);

        request
                .getRequestDispatcher("/WEB-INF/views/trackList.jsp")
                .include(request, response);

    }

}
