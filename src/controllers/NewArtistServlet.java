package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ArtistDAO;
import models.Artist;

@WebServlet("/new_artist")
public class NewArtistServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ArtistDAO artistDao = new ArtistDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/newArtist.jsp").include(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("artistName");
        Artist myNewArtist = new Artist(name);

        artistDao.storeArtist(myNewArtist);
        response.getWriter().println("Created a new artist " + myNewArtist.getId());
    }

}