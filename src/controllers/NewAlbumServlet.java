package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.AlbumDAO;
import db.ArtistDAO;
import models.Album;
import models.Artist;

@WebServlet("/new_album")
public class NewAlbumServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private AlbumDAO albumDao = new AlbumDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/newAlbum.jsp").include(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("albumTitle");
        Album myNewAlbum = new Album(title);

        albumDao.storeAlbum(myNewAlbum, null);
        response.getWriter().println("Created a new album " + myNewAlbum.getId());
    }

}