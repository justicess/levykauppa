package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.AlbumDAO;
import db.ArtistDAO;
import models.Album;
import models.Artist;

@WebServlet("/artists")
public class ArtistListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ArtistDAO artistDao = new ArtistDAO();
	private AlbumDAO albumDao = new AlbumDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			if(request.getParameter("searchName") != null && request.getParameter("searchName") != ""){
		        String name = request.getParameter("searchName");
		        List<Artist> artists = artistDao.searchArtist(name);
		        request.setAttribute("artists", artists);
				
		       
			}else if (request.getParameter("searchAlbums") != null && request.getParameter("searchAlbums") != ""){
		        String title = request.getParameter("searchAlbums");
		        List<Album> albums = albumDao.searchAlbums(title);
		        request.setAttribute("albums", albums);
			}else{
				List<Artist> artists = artistDao.findAllArtists();
				request.setAttribute("artists", artists);
			}
			request
			.getRequestDispatcher("/WEB-INF/views/artistList.jsp")
			.include(request, response);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("artistName");
        Artist myNewArtist = new Artist(name);

        artistDao.storeArtist(myNewArtist);
        response.getWriter().println("Created a new artist " + myNewArtist.getId());
        
        
    }

}
