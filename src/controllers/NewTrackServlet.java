package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.TrackDAO;
import models.Track;

@WebServlet("/new_track")
public class NewTrackServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private TrackDAO trackDao = new TrackDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/newTrack.jsp").include(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("TrackName");
        Track myNewTrack = new Track(name);

        trackDao.storeTrack(myNewTrack);
        response.getWriter().println("Created a new track " + myNewTrack.getId());
    }

}