package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.AlbumDAO;
import db.ArtistDAO;
import models.Album;
import models.Artist;

@WebServlet("/albums")
public class AlbumListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private AlbumDAO albumDao = new AlbumDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Album> albums = albumDao.findAllAlbums();
		
		request.setAttribute("albums", albums);
		
		request
			.getRequestDispatcher("/WEB-INF/views/albumList.jsp")
			.include(request, response);
		
	}

}
