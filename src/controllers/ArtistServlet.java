package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.AlbumDAO;
import db.ArtistDAO;
import models.Album;
import models.Artist;

@WebServlet("/artist")
public class ArtistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ArtistDAO artistDao = new ArtistDAO();
	private AlbumDAO albumDao = new AlbumDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		long artistid = Long.parseLong(request.getParameter("id"));
		// PrintWriter writer = response.getWriter();
		Artist artist = artistDao.findArtist(artistid);
		List<Album> albums = albumDao.findAlbumsByArtist(artistid);
		
			request.setAttribute("artist", artist);
			request.setAttribute("albums", albums);
			// writer.println(artist.getName());
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/artist.jsp");
			dispatcher.include(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        long artistid = Long.parseLong(request.getParameter("artistid"));
		String title = request.getParameter("albumTitle");
        Album myNewAlbum = new Album(title);

        long albumId = albumDao.storeAlbum(myNewAlbum, artistid);

        response.getWriter().println("Created a new album with the ID " + albumId);


        
        
    }
}
